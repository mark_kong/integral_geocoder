from geopy.geocoders import Nominatim, GoogleV3
from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseServerError
from django.views.decorators.csrf import csrf_exempt
import json
import os
from time import sleep
import time


@csrf_exempt
def jll(request):
    """Handle request using jll credentials."""
    return handleRequest(request, True)


@csrf_exempt
def index(request):
    """Handle request without using jll credentials."""
    return handleRequest(request, False)


def handleRequest(request, jll=False):
    """Given request and boolean parameter, true if jll request, return result
    Given post request with json with 'addresses' list or get request with
    address=, return geocode results as json. The 'addressField' sets an
    optional parameter defining the field to use to geocode, default is
    'address'."""
    if request.method == 'POST':
        received_json = json.loads(request.body)
        trans_start = time.time()
        if "addressField" in received_json:
            addressField = received_json["addressField"]
        else:
            addressField = "address"
        print "addressField: {}".format(addressField)
        for obj in received_json['addresses']:
            start = time.time()
            try:
                print obj[addressField]
                obj["result"] = geocode(obj[addressField], jll)
            except Exception as e:
                print "geocoder failure: {}".format(e)
                limitStr = "The given key has gone over the requests limit in the 24 hour period or has submitted too many requests in too short a period of time."
                if e.message == limitStr:
                    obj["result"] = "Geocoder limit exceeded. Try fewer addresses."
                else:
                    obj["result"] = "Invalid Address"
            duration = time.time() - start
            print "duration= {}".format(duration)
            if duration > 0.1:
                delay = 0
            else:
                delay = 0.1 - duration
                # print "delay= {}".format(delay)
                sleep(delay)
        print "total time: {}".format(time.time() - trans_start)
    return JsonResponse(json.dumps(received_json['addresses']), safe=False)


def geocode(address, useJllKeys=False):
    """Given address return geocoded result as json."""
    # geolocator = Nominatim()
    if useJllKeys:
        geolocator = GoogleV3(client_id=os.environ.get('jll_client_id'), secret_key=os.environ.get('jll_secret_key'))
    else:
        geolocator = GoogleV3()

    location = geolocator.geocode(address)
    return location.raw